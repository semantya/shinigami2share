# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0001_initial'),
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BuildAction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('title', models.CharField(max_length=256, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BuildModule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('title', models.CharField(max_length=256, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('title', models.CharField(max_length=256, blank=True)),
                ('mail_user', models.CharField(max_length=256, blank=True)),
                ('mail_pass', models.CharField(max_length=256, blank=True)),
                ('ga_code', models.CharField(max_length=256, blank=True)),
                ('customer', models.ForeignKey(related_name='domains', to='crm.Customer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProjectBuildDefaults',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
                ('value', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProjectBuildParameter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
                ('value', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProjectBuildStep',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('offset', models.PositiveIntegerField()),
                ('action', models.ForeignKey(related_name='instances', to='ci.BuildAction')),
                ('project', models.ForeignKey(related_name='build_steps', to='ci.Project')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProjectBuildTemplate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('offset', models.PositiveIntegerField()),
                ('action', models.ForeignKey(related_name='templates', to='ci.BuildAction')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProjectCollaborator',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('profile', models.ForeignKey(related_name='collaborations', to='core.Profile')),
                ('project', models.ForeignKey(related_name='collaborators', to='ci.Project')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProjectDomain',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fqdn', models.CharField(max_length=512)),
                ('project', models.ForeignKey(related_name='domains', to='ci.Project')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProjectType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('title', models.CharField(max_length=256, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='projectbuildtemplate',
            name='template',
            field=models.ForeignKey(related_name='build_steps', to='ci.ProjectType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='projectbuildparameter',
            name='step',
            field=models.ForeignKey(related_name='parameters', to='ci.ProjectBuildStep'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='projectbuilddefaults',
            name='step',
            field=models.ForeignKey(related_name='parameters', to='ci.ProjectBuildTemplate'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='project',
            name='template',
            field=models.ForeignKey(related_name='projects', to='ci.ProjectType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='buildaction',
            name='module',
            field=models.ForeignKey(related_name='actions', to='ci.BuildModule'),
            preserve_default=True,
        ),
    ]
