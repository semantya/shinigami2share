#-*- coding: utf-8 -*-

from myopla.shortcuts import *

from myopla.ci.models import *

##############################################################################

@login_required
def listing(request):
    return render(request, 'rest/template/list.html', dict(
        listing = ProjectType.objects.order_by('name'),
    ))

@login_required
def overview(request, narrow):
    obj = ProjectType.objects.get(name=narrow)

    if obj:
        return render(request, 'rest/template/view.html', dict(
            project = obj,
        ))
    else:
        return render(request, 'not-found.html', dict(
            narrow = narrow,
        ))
