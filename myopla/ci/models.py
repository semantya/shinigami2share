#-*- coding: utf-8 -*-

from myopla.shortcuts import *

from myopla.core.models import *
from myopla.crm.models import *

from .utils import Flavor

################################################################################

class BuildModule(models.Model):
    name      = models.CharField(max_length=64)
    title     = models.CharField(max_length=256, blank=True)

    def __str__(self):
        return self.title or self.name

#*******************************************************************************

class BuildAction(models.Model):
    module    = models.ForeignKey(BuildModule, related_name='actions')
    name      = models.CharField(max_length=64)
    title     = models.CharField(max_length=256, blank=True)

    def __str__(self):
        return str("[%s] %s" % (self.module.name, self.title or self.name))

    def __unicode__(self):
        return unicode("[%s] %s" % (self.module.name, self.title or self.name))

################################################################################

class ProjectType(models.Model):
    name      = models.CharField(max_length=64)
    title     = models.CharField(max_length=256, blank=True)

    def __str__(self):
        return self.title or self.name

#*******************************************************************************

class ProjectBuildTemplate(models.Model):
    template  = models.ForeignKey(ProjectType, related_name='build_steps')
    offset    = models.PositiveIntegerField()
    action    = models.ForeignKey(BuildAction, related_name='templates')

#*******************************************************************************

class ProjectBuildDefaults(models.Model):
    step      = models.ForeignKey(ProjectBuildTemplate, related_name='parameters')
    name      = models.CharField(max_length=128)
    value     = models.TextField()

################################################################################

class Project(models.Model):
    customer   = models.ForeignKey(Customer, related_name='projects')
    name       = models.CharField(max_length=64)

    title      = models.CharField(max_length=256, blank=True)

    #manager    = models.ForeignKey(Customer, related_name='managed_projects')

    #***************************************************************************

    mail_user  = models.CharField(max_length=256, blank=True)
    mail_pass  = models.CharField(max_length=256, blank=True)

    ga_code    = models.CharField(max_length=256, blank=True)

    #***************************************************************************

    git_path   = property(lambda self: settings.FILES_DIR('workdir', self.customer.codename, 'vhosts', self.name))
    #git_url    = property(lambda self: "git@code.myopla.net:%s/%s.git" % (self.customer.codename, self.name))
    git_url    = property(lambda self: "git@bitbucket.org:myopla/%s.git" % self.name)

    @property
    def git_lab(self):
        resp = GitLab.createprojectuser(self.customer.codename, self.name)

        if False:
            resp = GitLab.createprojectuser(self.customer.codename, self.name,
                description            = self.title,
                default_branch         = 'master',
                issues_enabled         = True,
                merge_requests_enabled = True,
                wiki_enabled           = True,
                snippets_enabled       = True,
                public                 = True,
            )

        return resp

    #git_url   = property(lambda self: self.git_lab['link'])

    #***************************************************************************

    @property
    def git_repo(self):
        if not os.path.exists(self.git_path):
            os.system('git clone %s %s' % (self.git_url, self.git_path))

        repo = git.Repo(self.git_path)

        return repo

    #git_url   = property(lambda self: self.git_lab['link'])

    def git_commits(self, branch='master', limit=200):
        lst = []

        try:
            lst += obj.git_repo.iter_commits(branch, max_count=limit)
        except:
            pass

        return lst

    #***************************************************************************

    template  = models.ForeignKey(ProjectType, related_name='projects')

    @property
    def flavor(self):
        if not hasattr(self, '_flv'):
            setattr(self, '_flv', Flavor.detect(self))

        return self._flv

    #***************************************************************************

    def __str__(self):     return str(self.title or self.name)
    def __unicode__(self): return unicode(self.title or self.name)

    #***************************************************************************

    def save(self, *args, **kwargs):
        for k in ('user', 'pass'):
            if not self.__dict__['mail_'+k]:
                setattr(self, 'mail_'+k, uuid.uuid4().hex)

        return super(Project, self).save(*args, **kwargs)

#*******************************************************************************

class ProjectDomain(models.Model):
    project   = models.ForeignKey(Project, related_name='domains')
    fqdn      = models.CharField(max_length=512)

    def __str__(self):
        return self.fqdn

class ProjectCollaborator(models.Model):
    project   = models.ForeignKey(Project, related_name='collaborators')
    profile   = models.ForeignKey(Profile, related_name='collaborations')

    roles     = models.ManyToManyField(Role, related_name='collaborations')

    def __str__(self):
        return self.fqdn

class ProjectBuildStep(models.Model):
    project   = models.ForeignKey(Project,     related_name='build_steps')
    offset    = models.PositiveIntegerField()
    action    = models.ForeignKey(BuildAction, related_name='instances')

    def __str__(self):
        return "[%s]%2d) %s" % (self.project, self.offset, self.action)

class ProjectBuildParameter(models.Model):
    step      = models.ForeignKey(ProjectBuildStep,     related_name='parameters')
    name      = models.CharField(max_length=128)
    value     = models.TextField()
