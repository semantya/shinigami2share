from django.contrib import admin

from .models import *

################################################################################

class ModuleAdmin(admin.ModelAdmin):
    list_display = ('name', 'title')

admin.site.register(BuildModule, ModuleAdmin)

class ActionAdmin(admin.ModelAdmin):
    list_display = ('module', 'name', 'title')
    list_filter  = ('module__name',)

admin.site.register(BuildAction, ActionAdmin)

################################################################################

class TemplateBuildInline(admin.TabularInline):
    model = ProjectBuildDefaults

class TemplateBuildAdmin(admin.ModelAdmin):
    list_display = ('template', 'offset', 'action')
    list_filter  = ('template__name', 'action__module__name', 'action__name')
    inlines = (
        TemplateBuildInline,
    )

admin.site.register(ProjectBuildTemplate, TemplateBuildAdmin)

class TemplateAdmin(admin.ModelAdmin):
    list_display = ('name', 'title')

admin.site.register(ProjectType, TemplateAdmin)

#*******************************************************************************

class BuildParameterInline(admin.TabularInline):
    model = ProjectBuildParameter

class BuildStepAdmin(admin.ModelAdmin):
    list_display = ('project', 'offset', 'action')
    list_filter  = ('project__customer__codename', 'project__name', 'action__module__name', 'action__name')

    inlines = (
        BuildParameterInline,
    )

admin.site.register(ProjectBuildStep, BuildStepAdmin)

class BuildStepInline(admin.TabularInline):
    model = ProjectBuildStep

class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'customer', 'title', 'template', 'flavor', 'git_url')
    list_filter  = ('customer__codename',)

    inlines = (
        BuildStepInline,
    )

admin.site.register(Project, ProjectAdmin)
