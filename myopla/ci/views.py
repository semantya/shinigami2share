#-*- coding: utf-8 -*-

from myopla.shortcuts import *

from .models import *

##############################################################################

def homepage(request):
    return render(request, 'pages/homepage.html', dict(

    ))

def upcoming(request):
    return render(request, 'pages/calendar.html', dict(

    ))

##############################################################################

def dyndns_api(request):
    resp = {
        'status': 'unknown',
    }

    obj = HostNode.objects.get(name=request.GET.get('hostname'))

    if obj:
        if obj.api_login==request.REQUEST.get('username'):
            if obj.api_passwd==request.REQUEST.get('password'):
                host = request.META['REMOTE_HOST']
                addr = request.META['REMOTE_ADDR']

                resp = {
                    'status':  'ok',
                    'message': 'IP address updated.',
                    'attrs':   dict(
                        hostname = obj.fqdn,
                        address  = addr,
                    ),
                }
        else:
            resp = {
                'status':    'resource',
                'data': obj.__dict__,
            }

            del resp['data']['_state']
    else:
        resp = {
            'status':    'not-found',
            'data_type': 'hostname',
            'narrow':    request.GET.get('hostname'),
        }

    return resp

##############################################################################

def admin_keys(request):
    return HttpResponse('\n'.join([
        pki.pub_key
        for profile in Profile.objects.all()
        for pki in profile.ssh_keys.all()
    ]))
