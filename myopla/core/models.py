#-*- coding: utf-8 -*-

from myopla.shortcuts import *

class Role(models.Model):
    name  = models.CharField(max_length=64)
    title = models.CharField(max_length=256, blank=True)

    def __str__(self):
        return self.title or self.name

class Profile(models.Model):
    user        = models.OneToOneField(User, related_name='profile')
    roles       = models.ManyToManyField(Role, blank=True, related_name='profiles')

    pseudo      = property(lambda self: '%(username)s' % self.user.__dict__)
    email       = property(lambda self: '%(email)s' % self.user.__dict__)
    full_name   = property(lambda self: '%(first_name)s %(last_name)s' % self.user.__dict__)

    avatar_path = property(lambda self: 'mugshots/%(username)s.jpg' % self.user.__dict__)

    @property
    def activities(self):
        return []

    def __str__(self):
        return str(self.full_name)

    def __unicode__(self):
        return unicode(self.full_name)

class Profile_SSH(models.Model):
    profile    = models.ForeignKey(Profile, related_name='ssh_keys')
    username   = models.CharField(max_length=64)

    pub_key    = models.TextField(blank=True)
    priv_key   = models.TextField(blank=True)
