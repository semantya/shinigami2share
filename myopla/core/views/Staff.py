#-*- coding: utf-8 -*-

from myopla.shortcuts import *

from myopla.core.models import *

##############################################################################

@login_required
def listing(request):
    return render(request, 'rest/team/list.html', dict(
        listing = Profile.objects.order_by('user__username'),
    ))

@login_required
def overview(request, narrow):
    obj = None

    if narrow=='me':
        obj = request.user.profile
    else:
        obj = Profile.objects.get(user__username=narrow)

    if obj:
        return render(request, 'rest/team/view.html', dict(
            personna = obj,
        ))
    else:
        return render(request, 'not-found.html', dict(
            narrow = narrow,
        ))

@login_required
def activity(request, narrow):
    obj = None

    if narrow=='me':
        obj = request.user.profile
    else:
        obj = Profile.objects.get(user__username=narrow)

    if obj:
        return render(request, 'rest/team/activity.html', dict(
            personna = obj,
        ))
    else:
        return render(request, 'not-found.html', dict(
            narrow = narrow,
        ))
