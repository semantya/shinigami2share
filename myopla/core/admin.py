from django.contrib import admin

from .models import *

class RoleAdmin(admin.ModelAdmin):
    list_display = ('name', 'title')

admin.site.register(Role, RoleAdmin)

class ProfileSSH(admin.TabularInline):
    model = Profile_SSH

class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'email', 'full_name')
    list_filter  = ('roles__name',)

    inlines = (
        ProfileSSH,
    )

admin.site.register(Profile, ProfileAdmin)
