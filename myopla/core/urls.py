from django.conf.urls import patterns, include, url

urlpatterns = patterns('myopla.core.views',
    url(r'^staff/$',                                        'Staff.listing',  name='staff_home'),
    url(r'^staff/(?P<narrow>.+)/$',                         'Staff.overview', name='staff_overview'),
    url(r'^staff/(?P<narrow>.+)/activity$',                 'Staff.overview', name='staff_activity'),

    url(r'^$',                                              'homepage'),
)
