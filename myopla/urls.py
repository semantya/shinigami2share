from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),

    url(r'^ci/',                include('myopla.ci.urls')),
    url(r'^crm/',               include('myopla.crm.urls')),
    url(r'^provision/',         include('myopla.provision.urls')),
    url(r'^',                   include('myopla.core.urls')),

    (r'^auth/login$',           'django.contrib.auth.views.login', {
        'template_name': 'auth/login.html',
    }),

    url(r'^nic/update$',        'myopla.provision.views.dyndns'),
)
