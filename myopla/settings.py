import os ; FILES_DIR = lambda *x: os.path.join(os.path.dirname(os.path.dirname(__file__)), 'files', *x)

PROVISION_ENDPOINT = 'localhost:8000'

PROVISION_DEFAULT_DIRs = (
    '/etc/supervisor/conf.d',
    '/etc/collectd',
    '/var/lib/supervisor',
    '/var/log/supervisor',
)

PROVISION_SYSTEM_CATALOG = [
    ('supervisor/supervisord.conf',       'supervisor/supervisord.conf'),
    ('supervisor/conf.d/logging.conf',    'supervisor/conf.d/logging.conf'),
    ('supervisor/conf.d/monitoring.conf', 'supervisor/conf.d/monitoring.conf'),

    ('collectd/collectd.conf',            'collectd/collectd.conf'),
]

PROVISION_USER_CATALOG = [
    ('log.io/harvester.conf', '.log.io/harvester.conf'),
]

ASANA_TOKEN   = 'eaeb3004187f730cc87807c4399893'

TRELLO_TOKEN  = 'eaeb3004187f730cc87807c4399893'

GITLAB_HOST   = 'code.myopla.net'
GITLAB_TOKEN  = 'eaeb3004187f730cc87807c4399893'

HIPCHAT_TOKEN = 'eaeb3004187f730cc87807c4399893'
HIPCHAT_OWNER = 1355197
HIPCHAT_ROOM  = 1088163

LANGUAGE_CODE = 'fr-FR'
TIME_ZONE = 'Africa/Casablanca'

SECRET_KEY = '#6*ng(=#ld7wl+d-f8%f-sm%zehtusng0^s%_#g7g#=-2iig8@'

DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'django_extensions',

    'myopla.core',
    'myopla.provision',
    'myopla.crm',
    'myopla.ci',
)

ROOT_URLCONF = 'myopla.urls'

WSGI_APPLICATION = 'myopla.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': FILES_DIR('main.sqlite3'),
    }
}

LOGIN_REDIRECT_URL = '/staff/me/'
LOGIN_URL          = '/auth/login'
LOGOUT_URL         = '/auth/logout'

STATICFILES_DIRS = (
    FILES_DIR('assets'),
)

STATIC_URL = '/static/'

STATIC_ROOT = FILES_DIR('static')

FIXTURE_DIRS = (
    FILES_DIR('fixtures'),
)

TEMPLATE_DIRS = (
    FILES_DIR('templates'),
)

USE_I18N = True
USE_L10N = True
USE_TZ = True

TEMPLATE_DEBUG = DEBUG

AUTH_PROFILE_MODEL = 'core.Profile'

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)
