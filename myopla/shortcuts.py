#-*- coding: utf-8 -*-

import os, sys, uuid, git

##############################################################################

from django.db import models
from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone as TZ

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

##############################################################################

from . import settings

#############################################################################

from hipster import Hipster
HipChat = Hipster(settings.HIPCHAT_TOKEN)

#############################################################################

import gitlab
GitLab = gitlab.Gitlab(settings.GITLAB_HOST, token=settings.GITLAB_TOKEN)

#############################################################################

from trello import TrelloApi
Trello = TrelloApi(settings.TRELLO_TOKEN)

#############################################################################

from asana import asana
Asana = asana.AsanaAPI(settings.ASANA_TOKEN, debug=True)
