#!/bin/bash

mkdir -p .myopla/logs

sh $* 1>.myopla/logs/stdout.log 2>.myopla/logs/stderr.log

for stream in stdout stderr ; do
    cat .myopla/logs/$stream.log | http POST "http://{{ endpoint }}/hosts/{{ host.name }}/process?stream=$stream&command=$*"
done

rm -fR .myopla/
