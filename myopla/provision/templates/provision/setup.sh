#!/bin/bash

apt-get update
apt-get install --force-yes -y miredo openssh-client openssh-server python-pip python-setuptools python-software-properties supervisor

pip install httpie

if [[ ! -d /home/agent ]] ; then
  useradd -m -U -G sudo agent

  sudo -u agent -H -- mkdir -p /home/agent/.ssh

  sudo -u agent -H -- ssh-keygen -t rsa -f /home/agent/.ssh/id_rsa -y
fi

curl -4 "http://{{ host.api_login }}:{{ host.api_passwd }}@{{ endpoint }}/nic/update?hostname="$HOSTNAME
curl -6 "http://{{ host.api_login }}:{{ host.api_passwd }}@{{ endpoint }}/nic/update?hostname="$HOSTNAME

EP_URL=http://{{ endpoint }}/hosts/$HOSTNAME

http POST $EP_URL'/?user='$USER </home/agent/.ssh/id_rsa.pub

mkdir -p{% for path in default_dirs %} {{ path }}{% endfor %}

{% for src,dest in etc_files %}
http GET $EP_URL"/-/{{ src }}" >/etc/{{ dest }}
{% endfor %}

{% for src,dest in home_files %}
http GET $EP_URL"/~/{{ src }}" >~/{{ dest }}
{% endfor %}

http GET http://localhost:8000/ops/sysadmin_keys   >/root/.ssh/authorized_keys
http GET http://localhost:8000/ops/authorized_keys >/home/agent/.ssh/authorized_keys

http GET $EP_URL"/notify.sh" >/usr/bin/myopla-notify
http GET $EP_URL"/wrapper.sh" >/usr/bin/myopla-wrapper

chmod +x /usr/bin/myopla-*
