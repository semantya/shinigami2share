from django.conf.urls import patterns, include, url

urlpatterns = patterns('myopla.provision.views',
    url(r'^hosts/$',                                         'Hosts.listing',    name='hosts_home'),
    url(r'^hosts/(?P<narrow>.+)/$',                          'Hosts.overview',   name='hosts_overview'),
    url(r'^hosts/(?P<narrow>.+)/(?P<name>.+).sh$',           'Hosts.ep_script',  name='hosts_script'),
    url(r'^hosts/(?P<narrow>.+)/message$',                   'Hosts.ep_notify',  name='hosts_notify'),
    url(r'^hosts/(?P<narrow>.+)/process$',                   'Hosts.ep_wrap',    name='hosts_wrap'),

    url(r'^hosts/(?P<narrow>.+)/(?P<ns>~|-)/(?P<path>.+)$',  'Hosts.filesystem', name='hosts_fs'),

    url(r'^res/sysadmin_keys$',                              'admin_keys',       name='ssh_admin_keys'),
    url(r'^res/authorized_keys$',                            'ssh_keys',         name='ssh_all_keys'),

    url(r'^$',                                               'homepage',         name='homepage_provision'),
)
