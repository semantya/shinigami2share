#-*- coding: utf-8 -*-

from myopla.shortcuts import *

from myopla.provision.models import *

##############################################################################

@login_required
def listing(request):
    return render(request, 'rest/host/list.html', dict(
        listing = HostNode.objects.order_by('name'),
    ))

@csrf_exempt
def overview(request, narrow):
    if request.method=='POST':
        obj, st = HostNode.objects.get_or_create(
            name=narrow,
        )

        obj.save()

        pki, st = HostSSH.objects.get_or_create(
            hostnode = obj,
            username = request.GET.get('user'),
        )

        pki.pub_key = request.read()

        pki.save()

        return HttpResponse(str(obj.__dict__))
    else:
        obj = HostNode.objects.get(name=narrow)

        if obj:
            return render(request, 'rest/host/view.html', dict(
                host = obj
            ))
        else:
            return render(request, 'not-found.html', dict(
                narrow = narrow,
            ))

#*******************************************************************************

@csrf_exempt
def ep_notify(request, narrow):
    obj = HostNode.objects.get(name=narrow)

    if obj:
        if request.method=='POST':
            resp = obj.notify(request.read(), level=request.GET.get('level', 'info'))

            return HttpResponse(str(resp))
        else:
            return render(request, 'rest/host/message.html', dict(

            ))
    else:
        return render(request, 'not-found.html', dict(
            narrow = narrow,
        ))

#*******************************************************************************

@csrf_exempt
def ep_wrap(request, narrow):
    obj = HostNode.objects.get(name=narrow)

    if obj:
        if request.method=='POST':
            cmd = HostCommand(
                hostnode = obj,
                when     = TZ.now(),
                stream   = request.GET.get('stream'),
                command  = request.GET.get('command'),
                content  = request.read(),
            )

            if len(cmd.content):
                cmd.save()

            return HttpResponse(str(cmd.__dict__))
        else:
            return render(request, 'rest/host/message.html', dict(

            ))
    else:
        return render(request, 'not-found.html', dict(
            narrow = narrow,
        ))

#*******************************************************************************

def ep_script(request, narrow, name):
    obj = HostNode.objects.get(name=narrow)

    if obj:
        return render(request, 'provision/%s.sh' % name, dict(
            host         = obj,
            endpoint     = settings.PROVISION_ENDPOINT,
            default_dirs = settings.PROVISION_DEFAULT_DIRs,
            etc_files    = settings.PROVISION_SYSTEM_CATALOG,
            home_files   = settings.PROVISION_USER_CATALOG,
        ))
    else:
        return render(request, 'not-found.html', dict(
            narrow = narrow,
        ))

def filesystem(request, narrow, ns, path):
    obj = None

    try:
        obj = HostNode.objects.get(name=narrow)
    except:
        obj = None

    if obj:
        NAMESPACEs = {
            '~': 'user',
            '-': 'system',
        }

        if ns in NAMESPACEs:
            return render(request, 'provision/%s/%s' % (NAMESPACEs[ns], path), dict(
                host = obj,
            ))
        else:
            return render(request, 'not-found.html', dict(
                narrow = narrow,
            ))
    else:
        return render(request, 'not-found.html', dict(
            narrow = narrow,
        ))
