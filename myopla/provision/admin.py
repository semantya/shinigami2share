from django.contrib import admin

from .models import *

class NodeProcess(admin.TabularInline):
    model = HostCommand

class CommandAdmin(admin.ModelAdmin):
    list_display  = ('hostnode', 'when', 'command', 'stream', 'content')

admin.site.register(HostCommand, CommandAdmin)

class NodeSSH(admin.TabularInline):
    model = HostSSH

class NodeAdmin(admin.ModelAdmin):
    list_display  = ('name', 'fqdn', 'ip_addr4', 'ip_addr6', 'api_login', 'api_passwd')
    list_editable = ('fqdn', 'ip_addr4', 'ip_addr6')

    inlines = (
        NodeSSH,
        NodeProcess,
    )

admin.site.register(HostNode, NodeAdmin)
