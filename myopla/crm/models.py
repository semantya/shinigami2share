#-*- coding: utf-8 -*-

from myopla.shortcuts import *

from myopla.core.models import *

################################################################################

class City(models.Model):
    name    = models.CharField(max_length=128)
    region  = models.CharField(max_length=512, blank=True)
    country = models.CharField(max_length=64)

    def __str__(self):
        if self.region:
            return "%(name)s - %(region)s [%(country)s]" % self.__dict__
        else:
            return "%(name)s [%(country)s]" % self.__dict__

class Customer(models.Model):
    codename          = models.CharField(max_length=128)
    email             = models.EmailField()

    brand_name        = models.CharField(max_length=256, blank=True)
    summary           = models.CharField(max_length=512, blank=True)
    address           = models.TextField(blank=True)
    city              = models.ForeignKey(City, related_name='customers', blank=True)

    avatar_path       = property(lambda self: 'brands/%(codename)s.jpg' % self.__dict__)

    leader            = models.ForeignKey(Profile, related_name='leaders')
    chief_technical   = models.ForeignKey(Profile, related_name='projects')
    chief_design      = models.ForeignKey(Profile, related_name='designs')

    sales_team        = models.ManyToManyField(Profile, related_name='sales', blank=True)

    def __str__(self):
        return str(self.brand_name or self.codename)

    def __unicode__(self):
        return unicode(self.brand_name or self.codename)

################################################################################

ISSUE_LEVELs = (
    ('trivial',  "Trivial"),
    ('minor',    "Minor"),
    ('major',    "Major"),
    ('critical', "Critical"),
    ('blocker',  "Blocker"),
)

ISSUE_NATUREs = (
    ('bug',         "Bug"),
    ('proposal',    "Proposal"),
    ('enhancement', "Enchancement"),
    ('hardware',    "Hardware malfunction"),
)

class TrackerIssue(models.Model):
    customer          = models.ForeignKey(Customer, related_name='issues')

    title             = models.CharField(max_length=512)
    content           = models.TextField()

    level             = models.CharField(max_length=24, choices=ISSUE_LEVELs, default='major')
    nature            = models.CharField(max_length=24, choices=ISSUE_NATUREs, default='bug')

    reporter          = models.ForeignKey(Profile, related_name='reported_issues')
    assignee          = models.ForeignKey(Profile, related_name='assigned_issues', blank=True, null=True, default=None)
    watchers          = models.ManyToManyField(Profile, related_name='watched_issues', blank=True)

class TrackerComment(models.Model):
    issue             = models.ForeignKey(TrackerIssue, related_name='comments')
    author            = models.ForeignKey(Profile, related_name='comments')
    message           = models.TextField()
