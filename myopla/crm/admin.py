from django.contrib import admin

from .models import *

class CityAdmin(admin.ModelAdmin):
    list_display   = ('name', 'region', 'country')
    list_filter    = ('country', 'region',)

admin.site.register(City, CityAdmin)

class CustomerAdmin(admin.ModelAdmin):
    list_display   = ('codename', 'email', 'brand_name', 'address', 'city', 'leader', 'chief_technical', 'chief_design')
    list_filter    = ('city','leader__user','chief_technical__user','chief_design__user', 'sales_team__user')

admin.site.register(Customer, CustomerAdmin)

class IssueComment(admin.TabularInline):
    model = TrackerComment

class IssueAdmin(admin.ModelAdmin):
    list_display   = ('customer', 'title', 'level', 'nature', 'reporter','assignee')
    list_filter    = ('customer__codename','level', 'nature','reporter__user','assignee__user', 'watchers__user')
    list_editable  = ('title', 'level', 'nature', 'reporter','assignee')

    inlines = (
        IssueComment,
    )

admin.site.register(TrackerIssue, IssueAdmin)
