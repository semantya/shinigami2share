from django.conf.urls import patterns, include, url

urlpatterns = patterns('myopla.crm.views',
    url(r'^customer/$',                                     'Customer.listing',  name='customer_home'),
    url(r'^customer/(?P<narrow>.+)/$',                      'Customer.overview', name='customer_overview'),

    url(r'^upcoming/(?P<lookup>.+)$',                       'upcoming_events',   name='crm_upcoming'),
    url(r'^$',                                              'homepage',          name='homepage_crm'),
)
